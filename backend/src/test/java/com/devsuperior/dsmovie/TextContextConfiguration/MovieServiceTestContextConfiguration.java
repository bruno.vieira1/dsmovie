package com.devsuperior.dsmovie.TextContextConfiguration;

import org.springframework.context.annotation.Bean;

import com.devsuperior.dsmovie.services.MovieService;

public class MovieServiceTestContextConfiguration {
	
	@Bean
    public MovieService movieService() {
        return new MovieService();
    }

}
