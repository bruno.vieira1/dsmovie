package com.devsuperior.dsmovie.TextContextConfiguration;

import org.springframework.context.annotation.Bean;


import com.devsuperior.dsmovie.services.ScoreService;

public class ScoreServiceTestContextConfiguration {
	
	@Bean
    public ScoreService scoreService() {
        return new ScoreService();
    }

}
