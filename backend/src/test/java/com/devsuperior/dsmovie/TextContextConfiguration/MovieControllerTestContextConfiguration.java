package com.devsuperior.dsmovie.TextContextConfiguration;

import org.springframework.context.annotation.Bean;

import com.devsuperior.dsmovie.controllers.MovieController;

public class MovieControllerTestContextConfiguration {
	
	@Bean
    public MovieController movieController() {
        return new MovieController();
    }


}
