package com.devsuperior.dsmovie.controllersTest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

import com.devsuperior.dsmovie.controllers.ScoreController;
import com.devsuperior.dsmovie.dto.MovieDTO;
import com.devsuperior.dsmovie.dto.ScoreDTO;
import com.devsuperior.dsmovie.services.ScoreService;
import com.devsuperior.dsmovie.TextContextConfiguration.ScoreControllerTestContextConfiguration;
import com.devsuperior.dsmovie.builders.MovieBuilder;
import com.devsuperior.dsmovie.builders.ScoreDTOBuilder;


@SpringBootTest(classes=ScoreControllerTest.class)
@Import(ScoreControllerTestContextConfiguration.class)
public class ScoreControllerTest {
	
	@Autowired
	private ScoreController scoreController; 
	
	@MockBean
	ScoreService scoreService;
	
	@Test
	void saveScoreTest () {
		
		ScoreDTO scoreDto = ScoreDTOBuilder.umScore().build(); 
		
		MovieDTO movieExpected = new MovieDTO(MovieBuilder.umFilme().build());
		
		Mockito.when(scoreService.saveScore(scoreDto)).thenReturn(movieExpected);
		
		MovieDTO actualMovie = scoreController.saveScore(scoreDto);
		
		assertThat(actualMovie).isEqualTo(movieExpected);
		
	}

}
