package com.devsuperior.dsmovie.controllersTest;

import static org.assertj.core.api.Assertions.assertThat;


import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import com.devsuperior.dsmovie.controllers.MovieController;
import com.devsuperior.dsmovie.dto.MovieDTO;
import com.devsuperior.dsmovie.services.MovieService;
import com.devsuperior.dsmovie.TextContextConfiguration.MovieControllerTestContextConfiguration;
import com.devsuperior.dsmovie.builders.MovieBuilder;

@SpringBootTest(classes=ScoreControllerTest.class)
@Import(MovieControllerTestContextConfiguration.class)
public class MovieControllerTest {
	
	@Autowired 
	private MovieController movieController;
	
	@MockBean
	private MovieService movieService;
	
	@Test
	void findByIdTest() {
		Long id = 1L;
		MovieDTO expectedMovie = new MovieDTO(MovieBuilder.umFilme().comId(id).build());
		
		Mockito.when(movieService.findById(id)).thenReturn(expectedMovie);
		
		MovieDTO actualMovie = movieController.findById(id);
		
		assertThat(actualMovie).isEqualTo(expectedMovie);
				
	}

}
