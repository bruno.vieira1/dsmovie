package com.devsuperior.dsmovie.builders;

import com.devsuperior.dsmovie.entities.Movie;

public class MovieBuilder {
	
	private static final Long DEFAULT_ID = null;
	private static final String DEFAULT_TITLE = null;
	private static final Double DEFAULT_SCORE = null;
	private static final Integer DEFAULT_COUNT = null;
	private static final String DEFAULT_IMAGE = null;
	
	private Long id = DEFAULT_ID;
	private String title = DEFAULT_TITLE;
	private Double score = DEFAULT_SCORE;
	private Integer count = DEFAULT_COUNT;
	private String image = DEFAULT_IMAGE;
	
	private MovieBuilder() {
		
	}
	
	public static MovieBuilder umFilme() {
		return new MovieBuilder();
	} 
	
	public MovieBuilder comId(Long id) {
		this.id = id;
		return new MovieBuilder();
	} 
	
	public MovieBuilder comTitle(String title) {
		this.title = title;
		return new MovieBuilder();
	} 
	
	public MovieBuilder comScore(Double score) {
		this.score = score;
		return new MovieBuilder();
	} 
	
	public MovieBuilder comCount(Integer count) {
		this.count = count;
		return new MovieBuilder();
	} 
	
	public MovieBuilder comImage(String image) {
		this.image = image;
		return new MovieBuilder();
	} 
	
	public MovieBuilder mas() {
		return new MovieBuilder().comId(id).comTitle(title).comScore(score).comCount(count).comImage(image);
	}
	
	public Movie build () {
		Movie filme =  new Movie();
		filme.setId(id);
		filme.setTitle(title);
		filme.setScore(score);
		filme.setCount(count);
		filme.setImage(image);
		return filme;
	}
}
