package com.devsuperior.dsmovie.builders;

import com.devsuperior.dsmovie.dto.ScoreDTO;
import com.devsuperior.dsmovie.entities.Movie;

public class ScoreDTOBuilder {
	
	private static final Long DEFAULT_MOVIE_ID = null;
	private static final String DEFAULT_EMAIL = null;
	private static final Double DEFAULT_SCORE = null;
	
	
	private Long movieId = DEFAULT_MOVIE_ID;
	private String email = DEFAULT_EMAIL;
	private Double score = DEFAULT_SCORE;
	
	
	private ScoreDTOBuilder() {
		
	}
	
	public static ScoreDTOBuilder umScore() {
		return new ScoreDTOBuilder();
	} 
	
	public ScoreDTOBuilder comMovieId(Long id) {
		this.movieId = id;
		return new ScoreDTOBuilder();
	} 
	
	public ScoreDTOBuilder comEmail(String email) {
		this.email = email;
		return new ScoreDTOBuilder();
	} 
	
	public ScoreDTOBuilder comScore(Double score) {
		this.score = score;
		return new ScoreDTOBuilder();
	} 
	
	
	public ScoreDTOBuilder mas() {
		return new ScoreDTOBuilder().comMovieId(movieId).comEmail(email).comScore(score);
	}
	
	public ScoreDTO build () {
		ScoreDTO scoreDTO = new ScoreDTO();
		scoreDTO.setMovieId(movieId);
		scoreDTO.setEmail(email);
		scoreDTO.setScore(score);
		return scoreDTO;
	}

}
