package com.devsuperior.dsmovie.builders;

import com.devsuperior.dsmovie.entities.User;

public class UserBuilder {
	
	private static final Long DEFAULT_ID = null;
	private static final String DEFAULT_EMAIL = null;
	
	
	
	private Long id = DEFAULT_ID;
	private String email = DEFAULT_EMAIL;
	
	private UserBuilder() {
		
	}
	
	public static UserBuilder umUser() {
		return new UserBuilder();
	} 
	
	public UserBuilder comId(Long id) {
		this.id = id;
		return new UserBuilder();
	} 
	
	public UserBuilder comEmail(String email) {
		this.email = email;
		return new UserBuilder();
	} 
	
	
	public UserBuilder mas() {
		return new UserBuilder().comId(id).comEmail(email);
	}
	
	public User build () {
		User user = new User();
		user.setId(id);
		user.setEmail(email);
		return user;
	}

}
