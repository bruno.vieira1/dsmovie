package com.devsuperior.dsmovie.servicesTest;

import static org.assertj.core.api.Assertions.assertThat;


import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

import com.devsuperior.dsmovie.dto.MovieDTO;
import com.devsuperior.dsmovie.entities.Movie;
import com.devsuperior.dsmovie.repositories.MovieRepository;
import com.devsuperior.dsmovie.services.MovieService;
import com.devsuperior.dsmovie.TextContextConfiguration.MovieServiceTestContextConfiguration;
import com.devsuperior.dsmovie.builders.MovieBuilder;

@SpringBootTest(classes=MovieServiceTest.class)
@Import(MovieServiceTestContextConfiguration.class)
public class MovieServiceTest {
	
	@MockBean
	private MovieRepository repositoryMock;
	
	@Autowired
	private MovieService movieService;
	
	
	@Test
	void testarFindById() {
		
		Movie filme = MovieBuilder.umFilme().comId(1L).build();
		
		MovieDTO filmeEsperado = new MovieDTO(filme);
		
		Mockito.when(repositoryMock.findById(filme.getId()).get()).thenReturn(filme);
		
		MovieDTO filmeAtual = movieService.findById(filme.getId());
				
		assertThat(filmeAtual).isEqualTo(filmeEsperado);
	}

}
