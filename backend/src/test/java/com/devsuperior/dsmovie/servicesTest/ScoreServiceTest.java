package com.devsuperior.dsmovie.servicesTest;

import static org.assertj.core.api.Assertions.assertThat;


import org.junit.jupiter.api.Test;

import org.mockito.Mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

import com.devsuperior.dsmovie.dto.MovieDTO;
import com.devsuperior.dsmovie.dto.ScoreDTO;
import com.devsuperior.dsmovie.entities.Movie;
import com.devsuperior.dsmovie.entities.Score;
import com.devsuperior.dsmovie.entities.User;
import com.devsuperior.dsmovie.repositories.MovieRepository;
import com.devsuperior.dsmovie.repositories.ScoreRepository;
import com.devsuperior.dsmovie.repositories.UserRepository;
import com.devsuperior.dsmovie.services.ScoreService;

import com.devsuperior.dsmovie.TextContextConfiguration.ScoreServiceTestContextConfiguration;
import com.devsuperior.dsmovie.builders.MovieBuilder;
import com.devsuperior.dsmovie.builders.ScoreDTOBuilder;
import com.devsuperior.dsmovie.builders.UserBuilder;

@SpringBootTest(classes=ScoreServiceTest.class)
@Import(ScoreServiceTestContextConfiguration.class)
public class ScoreServiceTest {
	
	@MockBean
	private MovieRepository movieRepository;

	@MockBean
	private UserRepository userRepository;

	@MockBean
	private ScoreRepository scoreRepository;
	
	@Autowired
	private ScoreService scoreService;
	
	
	@Test
	void saveScoreTest() {
		
		ScoreDTO scoreDTO = ScoreDTOBuilder.umScore().comMovieId(1L).comEmail("davipires123@gmail.com").comScore(4.5).build();
		
		User user = UserBuilder.umUser().comEmail("davipires123@gmail.com").comId(22L).build();
		
		Movie movie = MovieBuilder.umFilme().comId(33L).build();
		
		MovieDTO movieExpected = new MovieDTO(movie);
		
		Mockito.when(userRepository.findByEmail(scoreDTO.getEmail())).thenReturn(user);
		
		Mockito.when(movieRepository.findById(scoreDTO.getMovieId()).get()).thenReturn(movie);
		
		Score score = new Score();
	
		score.setMovie(movie);
		score.setUser(user);
		score.setValue(scoreDTO.getScore());
		
		Mockito.when(scoreRepository.saveAndFlush(score)).thenReturn(score);
		
		Mockito.when(movieRepository.save(movie)).thenReturn(movie);
		
		MovieDTO actualMovie = scoreService.saveScore(scoreDTO);
		
		assertThat(actualMovie).isEqualTo(movieExpected);
		
		
	}
	
	

}
