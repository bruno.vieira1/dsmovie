package com.devsuperior.dsmovie.repositoryTest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;

import com.devsuperior.dsmovie.builders.UserBuilder;
import com.devsuperior.dsmovie.entities.User;
import com.devsuperior.dsmovie.repositories.UserRepository;




@DataJpaTest
public class UserRepositoryTest {
	
	@Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;
	
    @Test
	void findByEmailTest() {
		String email = "davi@gmail.com";
		User userGiven = UserBuilder.umUser().comEmail(email).build();
		
		entityManager.persist(userGiven);
		entityManager.flush();
		
		User userReceived = userRepository.findByEmail(userGiven.getEmail());
		
		assertThat(userReceived).isEqualTo(userGiven);
	}

}
