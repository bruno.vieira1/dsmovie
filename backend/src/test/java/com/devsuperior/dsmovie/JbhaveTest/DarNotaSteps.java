package com.devsuperior.dsmovie.JbhaveTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class DarNotaSteps {
	
	WebDriver driver;
	
	@BeforeAll
	void setup() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\DinoPc\\Downloads\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://brunodsmovie.netlify.app");
	}
	
	
	@SuppressWarnings("deprecation")
	@Given("nenhum usuario avaliou o filme")
	@Test
	public void pegarFilmeSemNota() throws InterruptedException {
		//String notaASerDada = "3";
		WebElement primeiroFilmeBotao = driver.findElement(By.xpath("//*[@id=\"root\"]/div[2]/div/div[1]/div/div/a"));
		primeiroFilmeBotao.click();
		String tituloFilme = driver.findElement(By.tagName("h3")).getText();
		driver.findElement(By.id("email")).sendKeys("emailAleatorio@gmail.com");
		/*Select select = (Select) driver.findElement(By.id("score"));
		select.deselectByVisibleText(notaASerDada);*/
		WebElement select = driver.findElement(By.id("score"));
		select.click();
		Thread.sleep(1000);
		select.sendKeys(Keys.DOWN);
		select.sendKeys(Keys.DOWN);
		select.sendKeys(Keys.ENTER);
		String notaEsperada = select.getText();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[text()='Salvar']")).click();
		Thread.sleep(1000);
		String filmeDesejado = "//h3[text()='"+ tituloFilme + "']";
		WebElement tituloFilmeDesejado = driver.findElement(By.xpath(filmeDesejado));
		WebElement containerFilmeDesejado = tituloFilmeDesejado.findElement(By.className("dsmovie-score-container"));
		String notaFinal = containerFilmeDesejado.findElement(By.className("dsmovie-score-value")).getText();
		assertThat(notaFinal).isEqualTo(notaEsperada);
		
	}
	
	
	@When("informada a nota $nota")
	public void darNotaFilme(Double nota) {
		
	}
	
	
	@Then("a nota final do filme sera $nota")
	public void consultarNota(Double notaEsperada) {
		
	}


}
