package com.devsuperior.dsmovie.JbhaveTest;

import java.util.List;

import org.jbehave.core.junit.JUnitStories;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.EmbedderControls;
import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.parsers.RegexStoryParser;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.core.steps.MarkUnmatchedStepsAsPending;
import org.junit.jupiter.api.Test;


public class DarNotaTest extends JBehaveAbstractTest{
	
	
	public DarNotaTest() {
		EmbedderControls embedderControls = configuredEmbedder().embedderControls();
		embedderControls.useStoryTimeouts("600");
	}
	
	
	@Override
	protected List<String> storyPaths() {
		return new StoryFinder().findPaths(CodeLocations.codeLocationFromClass(this.getClass()),
			Arrays.asList("**/dar-nota.story"), Arrays.asList(""));
	}
	
	
	@Override
	public InjectableStepsFactory stepsFactory() {
			return new InstanceStepsFactory(configuration(), new DarNotaSteps());
	}


}
