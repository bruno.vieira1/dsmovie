Narrativa:
Como um usuario
desejo dar uma nota ao filme
de modo que ao final possa saber a media de todas as notas dadas

Cenário: filme ainda esta sem nota
Dado que nenhum usuario avaliou o filme
Quando informada a nota 3
Entao a nota final do filme sera 3

Cenário: Mais de uma nota e dada
Dado que um ou mais usuarios avaliaram o filme
Quando informada a nota 3 por um usuario
E depois informada a nota 4 por outro usuario
Entao a nota final do filme sera a media das notas informadas (3.5) 